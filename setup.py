from os import path
try:  # for pip >= 10
    from pip._internal.req import parse_requirements
except ImportError:  # for pip <= 9.0.3
    from pip.req import parse_requirements
try:  # for pip >= 10
    from pip._internal.download import PipSession
except ImportError:  # for pip <= 9.0.3
    from pip.download import PipSession
from setuptools import setup, find_packages

install_reqs = parse_requirements(
    'requirements.txt',
    session=PipSession,
)

install_reqs = [str(ir.req) for ir in install_reqs]

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="mlboard_client",
    version='0.0.1',  # Required
    description='mlboard client',  # Optional
    long_description=long_description,  # Optional
    url='https://github.com/h4nyu/mlboard_client',
    author='Xinyuan Yao',
    author_email='yao.ntno@google.com',
    keywords='machine learning',
    packages=["mlboard_client"],
    install_requires=install_reqs,
    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },
    project_urls={
        'Bug Reports': 'https://github.com/h4nyu/mlboard_client/issues',
        'Funding': 'https://donate.pypi.org',
        'Source': 'https://github.com/h4nyu/mlboard_client',
    },
)
