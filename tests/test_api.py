import pytest
import random
from mlboard_client.api import Api


@pytest.mark.parametrize("host_url, expected", [
    ('http://api:5000', 'http://api:5000/query',),
    ('http://api:5000/api', 'http://api:5000/api/query',),
    ('http://api:5000/api/', 'http://api:5000/api/query',),
])
def test_writer(host_url, expected):
    api = Api(host_url)
    assert api._api_url == expected
